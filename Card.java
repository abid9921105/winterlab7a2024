public class Card{
	private String suit;
	private String value;
	
	
	
	
	public Card(String suit, String value){
		this.suit = suit;
		this.value = value;
		
	}
	public String getSuit(){
		return this.suit;
	}
	public String getValue(){
		return this.value;
	}
	public String toString(){
		return (getValue() + " of " + getSuit());
	}
	public double calculateScore(){
		double suitscore = 0;
		double valuescore = 0;
		double totalscore = 0;

		if(this.value.equals("Ace")){
			valuescore += 1.0;
		}
		else if(this.value.equals("Two")){
			valuescore += 2.0;
		}
		else if(this.value.equals("Three")){
			valuescore += 3.0;
		}
		else if(this.value.equals("Four")){
			valuescore += 4.0;
		}
		else if(this.value.equals("Five")){
			valuescore += 5.0;
		}
		else if(this.value.equals("Six")){
			valuescore += 6.0;
		}
		else if(this.value.equals("Seven")){
			valuescore += 7.0;
		}
		else if(this.value.equals("Eight")){
			valuescore += 8.0;
		}
		else if(this.value.equals("Nine")){
			valuescore += 9.0;
		}
		else if(this.value.equals("Ten")){
			valuescore += 10.0;
		}
		else if(this.value.equals("Jack")){
			valuescore += 11.0;
		}
		else if(this.value.equals("Queen")){
			valuescore += 12.0;
		}
		else {
			valuescore += 13.0;
		}
		if(this.suit.equals("Hearts")){
			suitscore += 0.4;
		}
		else if(this.suit.equals("Spades")){
			suitscore += 0.3;
		}
		else if(this.suit.equals("Diamonds")){
			suitscore += 0.2;
		} else {
			suitscore += 0.1;
		}
		
		
	totalscore = suitscore + valuescore;
	return totalscore;
	
	}
}