import java.util.Random;


public class Deck{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	public Deck(){
		
		this.rng = new Random(53);
		this.cards = new Card[52];
		this.numberOfCards = 52;
		
		String[] suits = {"Spades","Clubs","Hearts","Diamonds"};
		String[] values = {"Ace","Two","Three","Four","Five","Six","Seven","Eight","Nine","Ten","Jack","Queen","King"};
		
		
		for(int i= 0; i < values.length; i++){
			for(int p = 0; p < suits.length; p++){
				this.cards[i + 13*p] = new Card(suits[p], values[i]);
			}
		}
		}
		public int length(){
			
			return this.numberOfCards;
		}
		public Card drawTopCard(){
			this.numberOfCards --;
			return this.cards[this.numberOfCards];
		}
		public String toString(){
			String cardeck = "";
			for (int i = 0; i < this.numberOfCards; i++){
				cardeck += this.cards[i] + "\n";
				
			}
			return cardeck;
		}
		public void toShuffle(){
		
		for(int i = 0; i < cards.length; i++){ 
			int num = i + this.rng.nextInt(this.numberOfCards - i);
			Card carded = this.cards[i];
			this.cards[i] = this.cards[num];
			this.cards[num] = carded;
		}
		}


}
