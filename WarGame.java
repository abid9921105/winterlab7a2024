import java.util.Scanner;

	public class WarGame{
		public static void main(String[] args){
			Deck thedeck = new Deck();
			thedeck.toShuffle();
			
			
			int player1points = 0;
			int player2points = 0;
			
			int i = 0;
			while (i < thedeck.length()){
			
			Card card1 = thedeck.drawTopCard();
			Card card2 = thedeck.drawTopCard();

			System.out.println(card1 + " " + card1.calculateScore());
			System.out.println(card2 + " " + card2.calculateScore());
			
			
			System.out.println("Player 1 currently has " + player1points + " point(s).");
			System.out.println("Player 2 currently has " + player2points + " point(s).");
			System.out.println("______________________________________________________");
			if (card1.calculateScore() > card2.calculateScore()){
				System.out.println("Player 1 Wins : Card is higher value.");
				player1points++;
				System.out.println("Player 1 now has " + player1points + " point(s).");
				System.out.println("Player 2 now has " + player2points + " point(s).");
				System.out.println("______________________________________________________");
			} else {
				System.out.println("Player 2 Wins : Card is higher value");
				player2points++;
				System.out.println("Player 1 now has " + player1points + " point(s).");
				System.out.println("Player 2 now has " + player2points + " point(s).");
				System.out.println("______________________________________________________");
			}
			
			}
			if(player1points > player2points){
			System.out.println("Player 1 has won the game");
			} else {
			System.out.println("Player 2 has won the game");
			}
	}
	}